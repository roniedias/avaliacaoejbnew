package br.com.fiap.schedule;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;

import br.com.fiap.bean.UsuarioBeanLocal;
import br.com.fiap.entity.Usuario;

@Stateless
public class Agendador {
	
	@EJB
	UsuarioBeanLocal uBean;
	
	private List<Usuario> usuarios = new ArrayList<Usuario>();
	
	public Agendador() {}
	
	@Schedule(second="*/15", minute="*", hour="*") public void agendar() {
		usuarios = uBean.getAll();
		System.out.println("*** Usuarios sorteados ****");
		
		if(usuarios.size() > 0) {
			for(Usuario u : usuarios) {
				if(u.isSorteado()) {
					System.out.println(u.getNome());
				}
			}
		}
		else if(usuarios.size() == 1) {
			System.out.println("Apenas o usuário " + usuarios.get(0).getNome() + " na base de dados");
		}
		else {
			System.out.println("Nenhum usuário na base de dados");
		}
				 
	}

}
