package br.com.fiap.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "usuario", schema = "avaliacaoejb")
public class Usuario implements Serializable {
	
		
	private static final long serialVersionUID = -1720148959012981668L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	@Column
	private Integer id;
	
	@Column
	private String cpf;
	
	@Column
	private String nome;
		
	@Column(nullable = false, columnDefinition = "TINYINT(1)")
	private boolean sorteado;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isSorteado() {
		return sorteado;
	}
	public void setSorteado(boolean sorteado) {
		this.sorteado = sorteado;
	}
	
	@Override
	public String toString() {
		return "Usuario [id=" + id + ", cpf=" + cpf + ", nome=" + nome
				+ ", sorteado=" + sorteado + "]";
	}
	
	
}

