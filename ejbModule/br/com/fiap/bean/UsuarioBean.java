package br.com.fiap.bean;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.fiap.entity.Usuario;


@Stateless(mappedName = "usuarioBean")
@LocalBean
public class UsuarioBean implements UsuarioBeanLocal {

    public UsuarioBean() {}
    
    @PersistenceContext(unitName="fiapPU")
    private EntityManager em;

    @Override
    public void add(Usuario usuario){
    	em.persist(usuario);
    }
    
    @Override
    public List<Usuario> getAll(){
    	TypedQuery<Usuario> query = em.createQuery("select u from Usuario u", Usuario.class);
    	return query.getResultList();
    }
    
    @Override
    public Usuario findById(int id) {
    	Usuario usuario = em.find(Usuario.class, id);
    	return usuario;
    }
    
    @Override
    public void update(Usuario u) {
    	Usuario usuario = em.find(Usuario.class, u.getId());
    	usuario.setId(u.getId());
    	usuario.setCpf(u.getCpf());
    	usuario.setNome(u.getNome());
    	usuario.setSorteado(u.isSorteado());
    	em.merge(usuario);
    }
    
    
}
