package br.com.fiap.bean;

import java.util.List;
import javax.ejb.Local;
import br.com.fiap.entity.Usuario;

@Local
public interface UsuarioBeanLocal {
	void add(Usuario usuario);
	List<Usuario> getAll();
	Usuario findById(int id);
	void update(Usuario u);
}