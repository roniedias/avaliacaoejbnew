package br.com.fiap.ejbws;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebParam;
import javax.jws.WebService;

import br.com.fiap.bean.UsuarioBean;
import br.com.fiap.entity.Usuario;

@Stateless
@WebService(serviceName = "Cadastro", portName = "CadastroPort", endpointInterface = "br.com.fiap.ejbws.Cadastro", targetNamespace = "http://jaxws.cadastro.fiap.com.br")
public class CadastroBean implements Cadastro {

	@EJB
	UsuarioBean usuarioBean;
	
	   @Override
		public String cadastrar(@WebParam(name = "usuario") Usuario usuario) {
		   try{
			   //UsuarioBean usuarioBean = new UsuarioBean();
			   usuarioBean.add(usuario);
			
			   return "Inserido com Sucesso!!";
			} catch (Exception e) {
				return "Erro ao Inserir:  "+e.getMessage();
			}
		}
}
