package br.com.fiap.ejbws;

import javax.ejb.Remote;
import javax.jws.WebService;

import br.com.fiap.entity.Usuario;

@Remote
@WebService(name = "CadastroPortType", targetNamespace = "http://jaxws.cadastro.fiap.com.br")
public interface Cadastro {
	String cadastrar(Usuario usuario) ;
}
